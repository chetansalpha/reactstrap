import React, { Component, Fragment } from 'react'

import Header from './Header.js'
import Footer from './Footer.js'
import HomeBanner from './HomeBanner.js'
import TrustPilot from './TrustPilot.js'
import PromoBanner from './PromoBanner.js'
import StickyBanner from './StickyBanner.js'
import RecentlyViewed from './RecentlyViewed.js'
import MostPopularHotels from './MostPopularHotels.js'
import MarketingBanners from './MarketingBanners.js'
import SignUp from './SignUp.js'

class Homepage extends Component {
    render() {
        return (
            <div id="homepage">
                <StickyBanner/>
                <Header/>
                <HomeBanner />
                <TrustPilot/>
                <PromoBanner/>
                <RecentlyViewed />
                <MostPopularHotels/>
                <MarketingBanners />
                <SignUp/>
                <Footer/>  
            </div>
        )
    }
}

export default Homepage