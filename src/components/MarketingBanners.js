import React, { Component, Fragment } from 'react'
import { Container,Col,Row } from 'reactstrap'
import data from '../data/data.json'

class MarketingBanners extends Component {
    render() {
    return (
        <div id="marketing-banners" className="margin-bottom large-padding">
            <Container>
                <h3>Inspiration for your next trip</h3>
                <Row>            
                    <Col lg={6} className="marketing-section">
                        {
                        data.MarketingBanner.map((MarketingBanner, i) => {
                            if(MarketingBanner.PriorityOrder == 1){
                                return (
                                    <Fragment key={i}>
                                        <a href={MarketingBanner.tagUrl}>
                                            <img src={MarketingBanner.ImageURL} alt={MarketingBanner.tagName} />
                                        </a>
                                        <div className="promotxt">
                                            <div className="promoname">
                                                <strong>{MarketingBanner.MarketingName}</strong>
                                            </div>
                                            <div className="promoprice">
                                                <p className="from">{MarketingBanner.Starting}</p>
                                                <p className="price">{MarketingBanner.Price}</p>
                                                <p className="pppn">{MarketingBanner.pppn}</p>
                                            </div>
                                        </div>
                                    </Fragment>
                                );
                            }   
                        })
                        }
                    </Col>
                    <Col lg={6} className="marketing-section d-sm-none d-lg-block d-none">
                        {
                        data.MarketingBanner.map((MarketingBanner, i) => {
                            if(MarketingBanner.PriorityOrder != 1){
                                return (
                                    <div className="margin-bottom promo-right" key={i}>
                                        <a href={MarketingBanner.tagUrl}>
                                            <img src={MarketingBanner.ImageURL} alt={MarketingBanner.tagName} />
                                        </a>
                                        <div className="promotxt">
                                            <div className="promoname">
                                                <strong>{MarketingBanner.MarketingName}</strong>
                                            </div>
                                            <div className="promoprice">
                                                <p className="from">{MarketingBanner.Starting}</p>
                                                <p className="price">{MarketingBanner.Price}</p>
                                                <p className="pppn">{MarketingBanner.pppn}</p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }   
                        })
                        }
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
}

export default MarketingBanners