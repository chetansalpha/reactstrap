import React from 'react';
import { Phone} from '@material-ui/icons';
import {
  header,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

class Header extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <header>
      <Navbar className="container" color="white" light expand="md">
        <NavbarBrand className="logo" href="/"></NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="#">Destinations</NavLink>
            </NavItem>            
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
              Group Booking
                </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Option 1
                  </DropdownItem>
                <DropdownItem>
                  Option 2
                </DropdownItem>
                <DropdownItem>
                  Option 3
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <NavLink href="#">Blog</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">FAQ</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">
                <div>Need help in booking? Call us </div>
                <Phone></Phone>
                0203 001 0109
              </NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
              Channel
                </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  United Kingdom
                </DropdownItem>
                <DropdownItem>
                  Ireland
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  }
}

export default Header