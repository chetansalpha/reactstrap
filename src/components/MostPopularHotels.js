import React, { Component, Fragment } from 'react'
import {Container, Row, Col} from 'reactstrap'
import Slider from "react-slick";

import data from '../data/data.json'

class MostPopularHotels extends Component {
    render() {
        var settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            initialSlide: 0,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  initialSlide: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };
        return (
            <div id="most-popular" className="margin-bottom large-padding">
                <Container>
                    <h3>Most Popular Destinations</h3>
                    <div className="popular-dest">
                    <Slider {...settings}>
                            {
                                data.PopularDestinationModel.map((PopularDestinationModel, i) => {
                                    return(
                                        <Fragment key={i}>
                                        <div className="popular-dest">
                                            <a href={PopularDestinationModel.DestinationUrl}>
                                                <img src={PopularDestinationModel.ImageURL} alt={PopularDestinationModel.DestinationName} />

                                                <p className="destName">
                                                    {PopularDestinationModel.DestinationName} <br/>
                                                    <span>from <b>{PopularDestinationModel.FromPrice}</b></span><span>pppn*</span>
                                                </p>
                                            </a>
                                        </div>
                                       
                                        </Fragment>
                                    );
                                })
                            }  
                             <div>
                                test
                            </div>                      
                        </Slider>
                    </div>
                </Container>
            </div>
        )
    }
}

export default MostPopularHotels
