import React, { Component, Fragment } from 'react'
import { Container } from 'reactstrap'

class FooterCopyright extends Component {
    render() {
        return (
            <Fragment>
                <div className="footer-copyright large-padding">
                    <Container>
                        <strong>
                        TRAVEL AWARE – STAYING SAFE AND HEALTHY ABROAD
                        </strong>
                        <p>
                            The Foreign &amp; Commonwealth Office and National Travel Health Network and Centre have up-to-date advice on staying safe and healthy abroad.
                        </p>
                        <p>
                            For the latest travel advice from the Foreign &amp; Commonwealth Office along with security and local laws, passport and visa information
                        </p>
                        <p>
                            click <a target="_blank" href="https://www.gov.uk/travelaware">www.gov.uk/travelaware</a> and follow @FCOtravel and <a target="_blank" href="https://www.gov.uk/foreign-travel-advice">https://www.gov.uk/foreign-travel-advice</a> and <a target="_blank" href="https://www.facebook.com/FCOtravel">Facebook.com/FCOtravel</a>
                        </p>
                        <p>
                            For more information please see <a target="_blank" href="https://www.alpharooms.com/faq">https://www.alpharooms.com/faq</a>
                        </p>
                        <p>
                            Keep up to date with current travel health news by visiting <a target="_blank" href="https://www.travelhealthpro.org.uk">www.travelhealthpro.org.uk</a>
                        </p>
                        <p>
                            Please note the advice can change so check regularly for updates.
                        </p>
                        <p>Bookings are arranged by Alpharooms.com (a member of the TTA (Q4586)), a trading name of Alpha Holidays Ltd. Products and prices are subject to availability and Alpharooms.com and the applicable supplier(s) terms &amp; conditions, and you must read these carefully before making any booking. The Alpha Holidays Limited ATOL Number is T7599. Each travel service is priced separately and independently, creating direct and separate contracts between you and the applicable supplier/principal(s). We are not a party to any such contract. However, where you book a “Multi-Contract Package” as defined in our Agency Terms of Business, we shall accept responsibility for your booking as the “organiser” under the Package Travel &amp; Linked Travel Arrangements Regulations 2018. This does not affect our agency status. Please see our <a target="_blank" href="https://www.alpharooms.com/agency-terms-of-business">agency terms of business</a> for further information. All ratings shown on this website are those of the supplier/principal and may not be the official classification. We cannot guarantee the accuracy of any ratings given.Many of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme. But ATOL protection does not apply to all holiday and travel services listed on this website. All Hotel only bookings are protected through the TTA. Please ask us to confirm what protection may apply to your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to: www.atol.org.uk/ATOLcertificate. All prices shown are accurate at the time they were posted on this website, but we act as an agent and do not control ever changing prices. Alpharooms.com reserves the right to withdraw offers at any time.
                        </p>
                    </Container>
                </div>
                <div className="divider"></div>
            </Fragment>            
        )
    }
}

export default FooterCopyright