import React, { Component } from 'react'
import { Container, Row, Col} from 'reactstrap';

const TrustPilot = () => {
    return(
        <div className="trust-logos margin-bottom large-padding">
            <Container>
                <Row>
                    <Col lg={3} md={4} sm={3} className="d-none d-lg-block d-md-block">
                        <div className="trustpilot-rtng center-text">
                            <i className="icon"></i>
                        </div>
                    </Col>
                    <Col lg={3} md={4} sm={3} className="d-none d-lg-block d-md-block">
                        <div className="ggle-rtng center-text">
                            <i className="icon"></i>
                        </div>
                    </Col>
                    <Col lg={3} md={3} sm={3} className="d-none d-lg-block d-md-none">
                        <div className="atol-bar center-text">
                            <i className="icon"></i>
                            <span><b>In-resort</b> <br/>Support</span>
                        </div>
                    </Col>
                    <Col lg={3} md={4} sm={3} className="d-none d-lg-block d-md-block">
                        <div className="deposit center-text">
                            <i className="dep-icon"></i>
                            <span><b>£20 Deposit</b><br/>on Hotel Bookings</span>                            
                        </div>
                    </Col>
                    <Col className="d-sm-block d-lg-none d-md-none trust-mobile">
                        <div className="trustpilot-rtng float-left">
                            <i className="icon"></i>
                        </div>
                        <div className="ggle-rtng float-left">
                            <i className="icon"></i>
                        </div>
                        <div className="deposit float-left">
                            <i className="dep-icon"></i>
                            <span><b>£20 Deposit</b><br/>on Hotel Bookings</span>                            
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default TrustPilot