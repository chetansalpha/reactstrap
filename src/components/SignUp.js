import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'

class SignUp extends Component {
    render() {
        return (
            <div className="signup-form margin-bottom large-padding">
                <Container>
                    <Row>
                        <Col lg={6} className="text-right">
                        <h4>Sign up to our emails to be the first to hear <br/>
                        <span className="newsltr-tagline">about our amazing deals, exclusive offers and news!</span></h4>
                        </Col>
                        <Col lg={6}>
                            <form>
                                <input type="text"></input>
                                <span className="btn-orange">Sign Up</span>
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default SignUp