import React, { Component } from 'react'

class StickyBanner extends Component {
    render() {
        return (
            <div className="sticky-banner">
                <a href="/tags/low-cost-hotel-deals.aspx">
                    <img className="d-none d-sm-none d-lg-block d-md-block" src="https://cdn4.alpharooms.com/assets-ux/content/images/home/D-topsticky-lastminuteDeals.jpg" alt="sticky-banner"/>
                    <img className="d-sm-block d-md-none" src="https://cdn4.alpharooms.com/assets-ux/content/images/home/M-topsticky-lastminuteDeals.jpg" alt="sticky-banner"/>
                </a>
            </div>
        )
    }
}

export default StickyBanner