import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'reactstrap'

class FooterLinks extends Component {
    render() {
        return (
            <Fragment>
                <div className="footer-links margin-bottom large-padding">
                    <Container>
                        <Row>
                            <Col lg={3} md={3} sm={3}>
                                <a href="/" className="logo"></a>
                            </Col>
                            <Col lg={3} md={3} sm={3}>
                                <strong>Get In Touch</strong>
                                <ul className="unstyled">
                                <li><a href="/contact-us" title="Contact Us">Contact Us</a></li>
                                <li><a href="/suppliers" title="Hotel Sign-Up">Hotel Sign-Up</a></li>
                                <li><a href="/group" title="Group Booking">Group Booking</a></li>
                                <li><a href="https://member.impactradius.com/campaign-campaign-info/alpharoomscom.brand" title="Affiliates">Affiliates</a></li>
                            </ul>
                            </Col>
                            <Col lg={3} md={3} sm={3}>
                                <strong>ALPHAROOMS</strong>
                                <ul className="unstyled">
                                    <li><a href="/myaccount/bookings" title="My Account">My Account</a></li>
                                    <li><a href="/transfersearch" title="Airport Transfers">Airport Transfers</a></li>
                                    <li><a href="/airportparkingsearch" title="Airport Parking">Airport Parking</a></li>
                                    <li><a href="/about-us" title="About Us">About Us</a></li>
                                    <li><a href="/price-promise" title="Price Promise">Price Promise</a></li>
                                    <li><a href="/security" title="Security">Security</a></li>
                                    <li><a href="/home/jobs" title="Jobs">Jobs</a></li>
                                </ul>
                            </Col>
                            <Col lg={3} md={3} sm={3}>
                                <strong>GENERAL</strong>
                                <ul className="unstyled">
                                    <li><a href="/agency-terms-of-business" title="Agency Terms of Business">Agency Terms of Business</a></li>
                                    <li><a href="/terms-of-use" title="Terms of Use">Terms of Use</a></li>
                                    <li><a href="/privacy" title="Privacy">Privacy</a></li>
                                    <li><a href="/cookies" title="Cookies">Cookies</a></li>
                                    <li><a href="/faq" title="FAQ">FAQ</a></li>
                                </ul>
                            </Col>
                        </Row>
                    </Container>                
                </div>
                <div className="divider"></div>
            </Fragment>
        )
    }
}

export default FooterLinks