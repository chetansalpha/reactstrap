import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'

class PromoBanner extends Component {
    render() {
        return (
            <div className="promo-banner margin-bottom">
                <Container>
                    <Row>
                        <img className="d-none d-lg-block" src="https://cdn4.alpharooms.com/assets-ux/content/images/home/D-easyJetBanner-Winter2020.jpg" />
                        <img className="d-sm-block d-md-none" src="https://cdn4.alpharooms.com/assets-ux/content/images/home/M-easyJetBanner-Winter2020.jpg" />
                    </Row>
                </Container>
            </div>
        )
    }
}

export default PromoBanner
