import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'

import RecentlyViewedCarousel from './RecentlyViewedCarousel.js'

class RecentlyViewed extends Component {
    render() {
        return (
            <div id="recently-viewed" className="margin-bottom large-padding">
                <Container>
                    <h3>Recently Viewed Hotels</h3>
                    <RecentlyViewedCarousel />
                </Container>
            </div>
        )
    }
}

export default RecentlyViewed