import React, { Component } from 'react'
import { Container } from 'reactstrap'

class FooterLogo extends Component {
    render() {
        return (
            <div className="footer-logo large-padding center-text">
                <Container>
                    Footer Logo
                </Container>
            </div>
        )
    }
}

export default FooterLogo