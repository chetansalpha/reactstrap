import React, { Fragment } from 'react'
import FooterCallus from './FooterCallus.js'
import FooterLinks from './FooterLinks.js'
import FooterCopyright from './FooterCopyright.js'
import FooterLogo from './FooterLogo.js'
    
const Footer = () =>{
    return(
        <footer>
            <FooterCallus/>
            <FooterLinks/>
            <FooterCopyright/>
            <FooterLogo/>
        </footer>
    )
}

export default Footer